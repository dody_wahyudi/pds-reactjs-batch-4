// soal 1 ( Membuat Function dengan return String )
function cetakFunction() {
  console.log("Hallo Nama saya Dody Wahyudi");
}

cetakFunction();

// soal 2 ( Membuat Function dengan rumus penjumlahan didalamnya)
function myFunction(angka1, angka2) {
  return angka1 + angka2;
}
let angka1 = 20;
let angka2 = 7;
let output = myFunction(angka1, angka2);

console.log(output);

// soal 3 ( Mengubah dalam bentuk arrow function )
const Hello = () => {
  return Hello;
};
console.log(Hello);

// soal 4 ( Memanggil key dalam sebuah object )
let obj = {
  nama: "john",
  umur: 22,
  bahasa: "indonesia",
};
console.log(obj.bahasa);

// soal 5 ( mengubah array menjadi object )
var mobil = [
  { merk: "BMW", warna: "merah", tipe: "sedan" },
  { merk: "toyota", warna: "hitam", tipe: "box" },
  { merk: "audi", warna: "biru", tipe: "sedan" },
];
console.log(mobil[0].merk);

let arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku", 1992];
let objDaftarPeserta = {
  nama: [0],
  jeniskelamin: [1],
  hobi: [2],
  tahunlahir: [3],
};
console.log(objDaftarPeserta);

// soal 6 ( Membuat sebuah array of object dan melakukan filter )


// soal 7 ( Destructuring pada Object )

// soal 8 ( Spread Operator pada Object )

// soal 9 ( Penggunaan Function dan Object )
