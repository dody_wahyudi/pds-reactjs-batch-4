// soal 1 ( Membuat Looping sederhana )
for (var angka = 0; angka < 10; angka++) {
  console.log(angka);
}

// soal 2 ( Membuat Looping dengan conditional angka ganjil )
for (var deret = 1; deret < 10; deret += 2) {
  console.log(deret);
}

// soal 3 ( Membuat Looping dengan conditional angka genap )
for (var deret = 0; deret < 10; deret += 2) {
  console.log(deret);
}

// soal 4 ( Mengakses element array )
let array1 = [1, 2, 3, 4, 5, 6]
console.log( array1[5] )

// soal 5 ( Mengurutkan element array )
let array2 = [5, 2, 4, 1, 3, 5] 
array2.sort()
console.log(array2)


// soal 6 ( Mengeluarkan element array )
let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"]
for(var i = 0; i < array3.length; i++) {
	       console.log(array3[i]);
}
         
// soal 7 ( Mengeluarkan element array dan dengan kondisi )
let array4 = [1, 2, 3, 4, 5, 6,7, 8, 9, 10]

for (let array4 = 0; array4 <= 10; array4 += 2) {
  console.log(array4);
}

// soal 8 ( menggabungkan element menjadi string )
let title = ["saya", "sangat", "senang", "belajar", "javascript"] 
let slug = title.join(" ")
console.log(slug)

// soal 9 ( Menambahkan element array )
var sayuran = []
sayuran.push("Kangkung","Bayam","Buncis","Kubis","Timun","Seledri","Tauge")
console.log(sayuran) 

